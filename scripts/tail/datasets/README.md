> _generated from: `./datasets/events-clients:1000-compression:1000000.csv`_

### Transactions sizes (bytes)

![](./plots/sizes.svg)

### Transactions amounts (ADA)

![](./plots/amounts.svg)

### Number of recipients

![](./plots/recipients.svg)

### Volume over time (ADA)

![](./plots/volume-ada.svg)

### Volume over time (USD)

![](./plots/volume-usd.svg)

### Number of transactions over time

![](./plots/density.svg)
